package com.example.class20;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ImageView imageView;
    int animationDuration = 1000;
    AnimationDrawable wifiAnimation;
    Button buttonToCanvas;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView) findViewById(R.id.imageView);
        ImageView imageDrawable = (ImageView) findViewById(R.id.image);
        text = (TextView) findViewById(R.id.textView);
        buttonToCanvas = (Button) findViewById(R.id.buttonToCanvas);


        Animation animation = AnimationUtils.loadAnimation(this, R.anim.bounce);
        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);

        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button.startAnimation(animation);
                text.startAnimation(animFadeIn);
                imageDrawable.setBackgroundResource(R.drawable.animation);
                wifiAnimation = (AnimationDrawable) imageDrawable.getBackground();
                ObjectAnimator animator = ObjectAnimator.ofFloat(imageView, "x", 600f);
                animator.setDuration(animationDuration);
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.playSequentially(animator);
                wifiAnimation.start();
                animatorSet.start();
            }
        });

        buttonToCanvas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(intent);

            }
        });

    }

    public void handleAnimation(View view) {


    }
}
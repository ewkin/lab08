package com.example.class20;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Paint;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Canvas;
import android.graphics.Bitmap;
import android.graphics.Rect;


public class MainActivity2 extends AppCompatActivity {
    private Context mContext;
    private Resources mResources;
    private RelativeLayout mRelativeLayout;
    private TextView mTextView;
    private Button mButton;
    private ImageView mImageView;

    private Context circleContext;
    private Resources circleResources;
    private RelativeLayout circleRelativeLayout;
    private TextView circleTextView;
    private ImageView circleImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mContext = getApplicationContext();

        mResources = getResources();

        mRelativeLayout = (RelativeLayout) findViewById(R.id.rl);
        mButton = (Button) findViewById(R.id.btn);
        mImageView = (ImageView) findViewById(R.id.iv);
        circleImageView = (ImageView) findViewById(R.id.circle);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = Bitmap.createBitmap(
                        500,
                        300,
                        Bitmap.Config.ARGB_8888
                );

                Bitmap circleBitmap = Bitmap.createBitmap(
                        500, // Width
                        300, // Height
                        Bitmap.Config.ARGB_8888 // Config
                );

                Canvas canvas = new Canvas(bitmap);

                Canvas circle = new Canvas(circleBitmap);

                canvas.drawColor(Color.LTGRAY);
                circle.drawColor(Color.LTGRAY);


                Paint paint = new Paint();
                paint.setStyle(Paint.Style.FILL);
                paint.setColor(Color.YELLOW);
                paint.setAntiAlias(true);

                int radius = Math.min(canvas.getWidth(),canvas.getHeight()/2);

                int padding = 50;

                Rect rectangle = new Rect(
                        padding,
                        padding,
                        canvas.getWidth() - padding,
                        canvas.getHeight() - padding
                );

                circle.drawCircle(
                        canvas.getWidth() / 2,
                        canvas.getHeight() / 2,
                        radius - padding,
                        paint // Paint
                );


                canvas.drawRect(rectangle,paint);

                mImageView.setImageBitmap(bitmap);
                circleImageView.setImageBitmap(circleBitmap);
            }
        });
    }
}